# Installation de Jenkins, Gitlab, Docker, Sonarqube, Maven  sur des machines distantes à partir d'une machine virtuelle Ansible

Ce projet contient plusieurs fichiers importants : le fichier __init.yml__ qui est le playbook principal à lancer, le fichier __requirements.yml__ qui va installer l'ensemble des rôles dans un dossier roles à la racine du projet.

## Pré-requis
      
* Des Vms tournant sous centos7 avec les caractéristiques suivantes pour tous les outils:


 - 2 Vcpu 
 - 2 Go de RAM
 - un client SSH installé 
 - 30 Go de stockage (varaible en fonction de l'utilisation)
 - 2 cartes réseaux : 
                        -  une carte réseau configurée en réseau privé Hôte (pour créer un sous-réseau entre les VMs hôtes)
                        -  une carte réseau configurée en NAT (qui se chargera de faire passer la connexion Internet à notre infrastructure)
                        
    Les différentes VMs seront les suivantes:
      - Ansible (pour le déploiement de notre infrastructure)
      - Gitlab (qui sera notre dépôt distant)
      - Jenkins (notre serveur de CI/CD)
      - Maven_git (qui combine Maven et git)
      - Docker (rôle pas encore défini)
      - Sonarqube (test de qualité du code)
    
    Les utilisateurs doivent avoir le même login. Il faut également ajouter l'utilisateur aux sudoers (/etc/sudoers). Il faut dans ce fichier spécifier que le mot de passe ne sera pas demandé. (NOPASSWD:ALL)

```sh
login ALL=(ALL) NOPASSWD:ALL
```

Remplacer *login* par le nom d'utilisateur utilisé.
## Informations 
- Chaque VM contient un utilisateur __root__ qui a pour défaut le mot de passe __password__. Il est important d'en tenir compte lors de la connexion en SSH. Pour vous connecter à partir d'une machine hôte Windows sur n'importe laquelle des machines vous pouvez soit passer par [PuTTY](https://www.putty.org/) soit utiliser Git Bash/Linux et vous connecter avec la commande suivante :

```sh
$ ssh user@IP_machine_distante
```
Avec *IP_machine_distante* l'IP de la machine flottante sur laquelle vous souhaitez vous connecter.

Si vous voulez ne pas écrire cette commande systématiquement, il faut ajouter une résolution DNS dans le fichier /etc/hosts de la machine Ansible.


      
## Instructions

1. Se placer dans le répertoire du projet ("StageCICD") avec Git Bash et lancer l'instruction :
      

```sh
$ ansible-galaxy install -r requirements.yml -p roles/
```



Cette instruction va installer l'ensemble des rôles sur la machine Ansible. Une fois que vous l'avez faite cette instruction, elle n'est plus à faire.

2. Exécuter l'application à partir de votre machine virtuelle Ansible en restant dans le répertoire du projet et en lançant cette instruction :

```sh
$ ansible-playbook init.yml
```

## Configuration Pare-feu
Par défaut, les services HTTP, SSH et SSL ne sont pas activées sur les machines déployées. Afin de les utiliser par la suite (pour accédér aux différents sites)
il faut autoriser le trafic en TCP/UDP sur le port ,__80__, __443__ et __22__. Effectuer la manipulation sur tous les pare-feu (iptables, firewalld)

Pour les services Sonar et Jenkins, il faut en plus ouvrir respectivement les ports __9000__ et les ports __8080__

## Inventaire Ansible
Si tout se passe bien, un dossier roles ainsi que l'ensemble des installations devraient être lancées sur les IP des hôtes spécifiées dans le fichier /etc/ansible/hosts de la machine virtuelle Ansible.
Désormais, il faut penser à éditer ce fichier afin d'ajouter les IP flottantes de chaque machine déployée.
Ouvrez donc ce fichier en root avec la commande : 

```sh
$ sudo vi /etc/ansible/hosts
```

Le format de ce fichier est le suivant : 

        [jenkins]
        ip_machine1

        [docker]
        ip_machine2

        [gitlab]

        ip_machine3
        
        [docker]
        ip_machine4

## Documentation
Pour de plus amples informations quant à la configuration pour utiliser chaque service de l'infrastructure, référez vous aux différents README crées dans les dossiers racines de chaque rôle. 

