#!/bin/bash

# Copy the JAR to the folder

cp -f java-app/target/*.jar jenkins/build

echo "***************"
echo "** Build des images Docker ***"
echo "***************"

cd jenkins/build/ && docker-compose -f docker-compose-build.yml build --no-cache