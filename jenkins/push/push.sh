#!/bin/bash 

echo "**************"
echo "** Push image ***********"

IMAGE="jenkins-maven"

echo "** Authentification *******"
docker login -u kirinraikage -p $PASS
echo "** Tag de l\'image *******"
docker tag $IMAGE:$BUILD_TAG kirinraikage/$IMAGE:$BUILD_TAG
echo "Push de l\'image'"
docker push kirinraikage/$IMAGE:$BUILD_TAG 