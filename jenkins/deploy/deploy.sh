#!/bin/bash

echo jenkins-maven > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $PASSWORD >> /tmp/.auth

echo "*******************"

echo "***** Copie du JAR dans la machine Docker ****"

sshpass -p $PASS_SSH scp -o StrictHostKeyChecking=no jenkins/build/*.jar prod-user@docker:/tmp

echo "**** Connexion à l'instance Docker ****"

ssh -i /opt/prod -T prod-user@docker

echo "**** Lancement du Dockerfile ****"

cd jenkins/deploy && docker-compose -f docker-compose-deploy.yml build --no-cache