#!/bin/bash

echo "*************************"
echo "** Création du jar **********"
echo "*************************"

WORKSPACE=/var/lib/jenkins/jobs/pipeline-docker-maven/workspace
docker run --rm -v $WORKSPACE/java-app:/app -v /root/.m2/:/root/.m2/ -w /app maven:alpine "$@"